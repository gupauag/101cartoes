package br.com.Cartoes.Cartao.controller;

import br.com.Cartoes.Cartao.models.Cartao;
import br.com.Cartoes.Cartao.models.dtos.CartaoEntradaDTO;
import br.com.Cartoes.Cartao.models.dtos.CartaoSaidaDTO;
import br.com.Cartoes.Cartao.models.dtos.CartaoSaidaGetDTO;
import br.com.Cartoes.Cartao.service.CartaoService;
import br.com.Cartoes.Cartao.models.dtos.MappingDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    MappingDTO mappingDTO = new MappingDTO();

    @PostMapping
    public ResponseEntity<CartaoSaidaDTO> criarCartao(@RequestBody CartaoEntradaDTO cartaoEntradaDTO){
        try {

            Cartao cartao = mappingDTO.mappingCartaoEntrada(cartaoEntradaDTO);
            CartaoSaidaDTO cartaoObjeto = mappingDTO.mappingCartaoDTO(cartaoService.criarCartao(cartao));
            return ResponseEntity.status(201).body(cartaoObjeto);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<CartaoSaidaDTO> ativarDesativarCartao(@PathVariable(name = "id") int id,
                                                        @RequestBody CartaoEntradaDTO cartaoEntradaDTO){
        try {
            Cartao cartao = mappingDTO.mappingCartaoEntrada(cartaoEntradaDTO);
            CartaoSaidaDTO cartaoObjeto = mappingDTO.mappingCartaoDTO(
                    cartaoService.ativarDesativarCartao(id, cartao.isAtivo()));
            return ResponseEntity.status(200).body(cartaoObjeto);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<CartaoSaidaGetDTO> consultaCartao(@PathVariable(name = "id") int id){
        try {
            CartaoSaidaGetDTO cartaoObjeto = mappingDTO.mappingCartaoDTOGet(cartaoService.consultaCartao(id));
            return ResponseEntity.status(200).body(cartaoObjeto);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
