package br.com.Cartoes.Cartao.service;

import br.com.Cartoes.Cartao.models.Cartao;
import br.com.Cartoes.Clientes.models.Cliente;
import br.com.Cartoes.Cartao.models.dtos.CartaoEntradaDTO;
import br.com.Cartoes.Cartao.repository.CartaoRepository;
import br.com.Cartoes.Clientes.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;


    public Cartao criarCartao(Cartao cartao){

        Optional<Cartao> optionalCartao= cartaoRepository.findAllByNumero(cartao.getNumero());
        if(optionalCartao.isPresent())
            throw new RuntimeException("Cartao já existente");

        Optional<Cliente> optionalCliente = clienteRepository.findById(cartao.getCliente().getId());
        if(optionalCliente.isPresent()){
            cartao.setCliente(optionalCliente.get());
            return cartaoRepository.save(cartao);
        }else
            throw new RuntimeException("Cliente inexistente");
    }

    public Cartao ativarDesativarCartao(int id, boolean ativo){
        Optional<Cartao> optionalCartao = consultaCartao(id);
        optionalCartao.get().setAtivo(ativo);
        return cartaoRepository.save(optionalCartao.get());
    }

    public Optional<Cartao> consultaCartao(int id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        if(optionalCartao.isPresent()){
            return optionalCartao;
        }else
            throw new RuntimeException("Cartão inexistente");
    }

}
