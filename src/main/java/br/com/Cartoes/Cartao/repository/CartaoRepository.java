package br.com.Cartoes.Cartao.repository;

import br.com.Cartoes.Cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

    Optional<Cartao> findAllByNumero(String numero);

}
