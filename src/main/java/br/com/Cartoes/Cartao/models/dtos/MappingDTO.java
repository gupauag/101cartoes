package br.com.Cartoes.Cartao.models.dtos;

import br.com.Cartoes.Cartao.models.Cartao;
import br.com.Cartoes.Clientes.models.Cliente;

import java.util.Optional;

public class MappingDTO {

    public CartaoSaidaDTO mappingCartaoDTO(Cartao cartao){
        CartaoSaidaDTO saidaDTO = new CartaoSaidaDTO();
        saidaDTO.setAtivo(cartao.isAtivo());
        saidaDTO.setClienteId(cartao.getId());
        saidaDTO.setNumero(cartao.getNumero());
        saidaDTO.setId(cartao.getId());
        return saidaDTO;
    }

    public CartaoSaidaGetDTO mappingCartaoDTOGet(Optional<Cartao> cartao){
        CartaoSaidaGetDTO saidaDTO = new CartaoSaidaGetDTO();
        saidaDTO.setClienteId(cartao.get().getId());
        saidaDTO.setNumero(cartao.get().getNumero());
        saidaDTO.setId(cartao.get().getId());
        return saidaDTO;
    }

    public Cartao mappingCartaoEntrada(CartaoEntradaDTO cartaoEntradaDTO){
        Cartao cartao = new Cartao();
        Cliente cliente = new Cliente();
        cliente.setId(cartaoEntradaDTO.getClienteId());
        cartao.setAtivo(cartaoEntradaDTO.isAtivo());
        cartao.setCliente(cliente);
        cartao.setNumero(cartaoEntradaDTO.getNumero());

        return cartao;

    }


}
