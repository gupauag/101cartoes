package br.com.Cartoes.Cartao.models.dtos;

public class CartaoSaidaGetDTO {

    private int id;
    private String numero;
    private int clienteId;

    public CartaoSaidaGetDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }
}
