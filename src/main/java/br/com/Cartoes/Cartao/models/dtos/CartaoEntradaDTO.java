package br.com.Cartoes.Cartao.models.dtos;

import javax.validation.constraints.NotNull;

public class CartaoEntradaDTO {

    @NotNull(message = "Numero não pode ser null")
    private String numero;

    private int clienteId;

    private boolean ativo;

    public CartaoEntradaDTO() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }


}
