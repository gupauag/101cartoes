package br.com.Cartoes.Pagamento.repository;

import br.com.Cartoes.Pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    Iterable<Pagamento> findAllByCartaoId(int idCartao);

}
