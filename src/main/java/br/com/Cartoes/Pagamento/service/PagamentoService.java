package br.com.Cartoes.Pagamento.service;

import br.com.Cartoes.Cartao.models.Cartao;
import br.com.Cartoes.Pagamento.models.DTO.PagamentoEntradaDTO;
import br.com.Cartoes.Pagamento.models.Pagamento;
import br.com.Cartoes.Cartao.repository.CartaoRepository;
import br.com.Cartoes.Pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    public Pagamento criarPagamento(PagamentoEntradaDTO pagamentoEntradaDTO){

        Optional<Cartao> optionalCartao = cartaoRepository.findById(pagamentoEntradaDTO.getCartao_id());
        if(optionalCartao.isPresent()){
            if(optionalCartao.get().isAtivo()) {
                Pagamento pagamento = new Pagamento();
                pagamento.setCartao(optionalCartao.get());
                pagamento.setDescricao(pagamentoEntradaDTO.getDescricao());
                pagamento.setValor(pagamentoEntradaDTO.getValor());

                return pagamentoRepository.save(pagamento);
            }else throw new RuntimeException("Cartão inativo, favor ativar cartao antes de efetuar o pagmento.");
        }else
            throw new RuntimeException("Cartão inexistente");
    }

    public Iterable<Pagamento> buscaTodosPagamentosDoCartao(int idCartao){
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(idCartao);
        return pagamentos;
    }





}
