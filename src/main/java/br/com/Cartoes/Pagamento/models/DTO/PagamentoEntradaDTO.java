package br.com.Cartoes.Pagamento.models.DTO;

import javax.validation.constraints.NotNull;

public class PagamentoEntradaDTO {

    @NotNull(message = "O numero do cartão é obrigatório")
    public int cartao_id;

    @NotNull(message = "Descrição não pode ser null")
    public String descricao;

    @NotNull(message = "Valor não pode ser null")
    public double valor;

    public PagamentoEntradaDTO() {
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
