package br.com.Cartoes.Pagamento.models.DTO;

import br.com.Cartoes.Pagamento.models.DTO.PagamentoSaidaDTO;
import br.com.Cartoes.Pagamento.models.Pagamento;

public class MappingDTO {

    public PagamentoSaidaDTO mappingPagamentoDTO(Pagamento pagamento){
        PagamentoSaidaDTO saidaDTO = new PagamentoSaidaDTO();
        saidaDTO.setCartao_id(pagamento.getCartao().getId());
        saidaDTO.setDescricao(pagamento.getDescricao());
        saidaDTO.setId(pagamento.getId());
        saidaDTO.setValor(pagamento.getValor());
        return saidaDTO;
    }

}
