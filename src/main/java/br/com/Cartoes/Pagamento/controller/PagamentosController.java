package br.com.Cartoes.Pagamento.controller;

import br.com.Cartoes.Pagamento.models.DTO.MappingDTO;
import br.com.Cartoes.Pagamento.models.DTO.PagamentoEntradaDTO;
import br.com.Cartoes.Pagamento.models.DTO.PagamentoSaidaDTO;
import br.com.Cartoes.Pagamento.models.Pagamento;
import br.com.Cartoes.Pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/")
public class PagamentosController {

    @Autowired
    private PagamentoService pagamentoService;

    MappingDTO mappingDTO = new MappingDTO();

    @PostMapping("pagamento")
    public ResponseEntity<PagamentoSaidaDTO> criarPagamento(@RequestBody PagamentoEntradaDTO pagamentoEntradaDTO){
        try {
            PagamentoSaidaDTO pagamentoObejeto = mappingDTO.mappingPagamentoDTO(
                    pagamentoService.criarPagamento(pagamentoEntradaDTO));
            return ResponseEntity.status(201).body(pagamentoObejeto);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("pagamentos/{id_cartao}")
    public ResponseEntity<Iterable<PagamentoSaidaDTO>> buscaTodosPagamentosDoCartao(@PathVariable(name = "id_cartao") int id_cartao){
        try {

            List<PagamentoSaidaDTO> listaPagamentos = new ArrayList<>();
            for (Pagamento pagamento: pagamentoService.buscaTodosPagamentosDoCartao(id_cartao)) {
                listaPagamentos.add(mappingDTO.mappingPagamentoDTO(pagamento));
            }
            return ResponseEntity.status(200).body(listaPagamentos);

        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }




}