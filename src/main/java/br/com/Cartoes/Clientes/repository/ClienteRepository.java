package br.com.Cartoes.Clientes.repository;

import br.com.Cartoes.Clientes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Integer> {
}
