package br.com.Cartoes.Clientes.service;

import br.com.Cartoes.Clientes.models.Cliente;
import br.com.Cartoes.Clientes.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente consultarClientePorId(int id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if(optionalCliente.isPresent())
            return optionalCliente.get();
        throw new RuntimeException("Cliente não foi encontrado");
    }
}
