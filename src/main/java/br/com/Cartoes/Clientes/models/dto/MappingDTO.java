package br.com.Cartoes.Clientes.models.dto;

import br.com.Cartoes.Clientes.models.Cliente;

public class MappingDTO {

    public Cliente mappingCliente(ClienteRequest clienteRequest){

        Cliente cliente = new Cliente();
        cliente.setId(clienteRequest.getId());
        cliente.setName(clienteRequest.getName());
        return cliente;
    }

}
