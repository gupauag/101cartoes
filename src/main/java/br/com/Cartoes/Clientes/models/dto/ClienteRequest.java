package br.com.Cartoes.Clientes.models.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ClienteRequest {

    private int id;

    @Size(min=5, max = 100, message = "O nome deve ser entre 5 e 100 caracteres")
    @NotNull(message = "O nome não pode ser null")
    private String name;

    public ClienteRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
