package br.com.Cartoes.Clientes.controller;

import br.com.Cartoes.Clientes.models.Cliente;
import br.com.Cartoes.Clientes.models.dto.ClienteRequest;
import br.com.Cartoes.Clientes.models.dto.MappingDTO;
import br.com.Cartoes.Clientes.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    MappingDTO mappingDTO = new MappingDTO();

    @PostMapping
    public ResponseEntity<Cliente> criarCliente(@RequestBody @Valid ClienteRequest clienteRequest){
        Cliente clienteObjeto = clienteService.criarCliente(mappingDTO.mappingCliente(clienteRequest));
        return ResponseEntity.status(201).body(clienteObjeto);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Cliente> consultarClientePorId(@PathVariable(name = "id") int id){
        try {
            Cliente clienteObjeto = clienteService.consultarClientePorId(id);
            return ResponseEntity.status(200).body(clienteObjeto);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
